from plum.structure import Member, Structure

from .headers import BaseKeyString


def createEntryForListIndex(header, index):
    indexName = header.indexName.decode(errors="ignore")
    entryName = f"{indexName}.Entry"
    fields = [
        (f"_{i}", keyInfo.getKeyClass(entryName))
        for i, keyInfo in enumerate(header.keyInfoArray)
    ]
    for field in fields:
        if issubclass(field[1], BaseKeyString):
            field[1].index = index
    return type(entryName, (Structure,), {f: Member(cls=kType) for f, kType in fields})
