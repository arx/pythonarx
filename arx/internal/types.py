from plum import InsufficientMemoryError, Plum, PlumType, UnpackError, getbytes
from plum.bytearray import ByteArray
from plum.int import Int
from plum.int.big import UInt8, UInt16, UInt32, UInt64
from plum.int.bitfields import BitField, BitFields
from plum.structure import Member, Structure

__all__ = [
    "UInt8",
    "UInt16",
    "UInt24",
    "UInt32",
    "UInt40",
    "UInt48",
    "UInt56",
    "UInt64",
    "PString",
    "ContentAddress",
    "EntryPos",
]


class UInt24(Int, nbytes=3, signed=False, byteorder="big"):  # type: ignore
    """Unsigned big endian 24 bit integer."""


class UInt40(Int, nbytes=5, signed=False, byteorder="big"):  # type: ignore
    """Unsigned big endian 40 bit integer."""


class UInt48(Int, nbytes=6, signed=False, byteorder="big"):  # type: ignore
    """Unsigned big endian 48 bit integer."""


class UInt56(Int, nbytes=7, signed=False, byteorder="big"):  # type: ignore
    """Unsigned big endian 56 bit integer."""


class Uuid(ByteArray, nbytes=16):  # type: ignore
    """UUID"""


class PStringType(PlumType):
    def __new__(cls, name, bases, namespace, nbytes=None):
        return super().__new__(cls, name, bases, namespace)

    def __init__(cls, name, bases, namespace, nbytes=None):
        super().__init__(name, bases, namespace)

        if nbytes is None:
            nbytes = cls.__nbytes__
        else:
            assert nbytes > 0
            assert nbytes <= 256

        cls.__nbytes__ = nbytes


class PString(bytearray, Plum, metaclass=PStringType):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.__nbytes__ is None:
            maxbytes = 255
        else:
            maxbytes = self.__nbytes__ - 1

        if len(self) > maxbytes:
            raise ValueError(
                f"cannot encode more than {maxbytes} bytes, got {len(self)}"
            )

    @classmethod
    def __unpack__(cls, buffer, offset, parent, dump):
        orig_offset = offset
        size, offset = UInt8.__unpack__(
            buffer,
            offset,
            parent,
            None if dump is None else dump.add_record(access="[0] (.size)", cls=UInt8),
        )
        if cls.__nbytes__ is None:
            maxSize = 255
        else:
            maxSize = cls.__nbytes__ - 1

        if size > maxSize:
            raise UnpackError(f"PString length({size}) is too long (max {maxSize})")

        try:
            data, offset = getbytes(buffer, offset, size, dump)
        except InsufficientMemoryError:
            if dump:
                dump.memory = b"".join(subdump.memory for subdump in dump)
                del dump[:]
            raise
        finally:
            if dump:
                unpacked_bytes = dump.memory
                dump.memory = b""
                for i in range(0, len(unpacked_bytes), 16):
                    chunk = unpacked_bytes[i : i + 16]
                    dump.add_record(
                        access=f"[{i}:{i + len(chunk)}]",
                        value=str(bytearray(chunk)),
                        memory=chunk,
                    )

            if cls.__nbytes__ is not None:
                offset = orig_offset + cls.__nbytes__
                if offset > len(buffer):
                    raise InsufficientMemoryError(
                        f"{offset-len(buffer)} too few bytes to unpack {cls.__name__} "
                        f"({cls.__nbytes__} needed, only {len(buffer)-orig_offset} available)"
                    )

        return bytearray(data), offset

    @classmethod
    def __pack__(cls, buffer, offset, parent, value, dump):
        # pylint: disable=too-many-arguments
        nbytes = cls.__nbytes__
        size = len(value)
        if nbytes is None:
            maxbytes = 255
            nbytes_short = 0
        else:
            maxbytes = nbytes - 1
            nbytes_short = maxbytes - len(value)

        if size > maxbytes:
            raise ValueError(
                f"expected length to be max {maxbytes} but instead found {len(value)}"
            )

        end = offset + 1 + len(value) + nbytes_short
        buffer[offset:end] = bytes([size]) + value + b"\x00" * nbytes_short

        if dump:
            dump.add_record(
                access="[0] (size)", value=str(size), memory=bytes([size]), cls=UInt8
            )
            for i in range(0, len(value), 16):
                chunk = value[i : i + 16]
                dump.add_record(
                    access=f"[{i}:{i + len(chunk)}]",
                    value=str(bytearray(chunk)),
                    memory=chunk,
                )

        return end


class HeaderFreeData(ByteArray, nbytes=21, fill=0):  # type: ignore
    pass


class PackInfoPathName(PString, nbytes=104):  # type: ignore
    pass


class PackInfoFreeData(ByteArray, nbytes=111, fill=0):  # type: ignore
    pass


class ContentAddress(Structure):
    packId: int = Member(cls=UInt8)
    contentId: int = Member(cls=UInt24)


class EntryPos(BitFields, nbytes=4, byteorder="big"):  # type: ignore
    blobNumber: int = BitField(size=12)
    clusterNumber: int = BitField(size=20)
