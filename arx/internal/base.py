import enum


class CompressionType(enum.IntEnum):
    NONE = 0
    LZ4 = 1
    LZMA = 2
    ZSTD = 3


class KeyType(enum.IntFlag):
    contentAddress = 0b0000
    uint = 0b0001
    sint = 0b0010
    char_array = 0b0011
    pstring = 0b0100
    localSize = 0b0001
    flookup = 0b0010
