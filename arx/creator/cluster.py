import lzma

import lz4.frame

from ..internal import ClusterHeader, CompressionType, to_bytes


class Cluster:
    def __init__(self):
        self.content = []
        self.currentSize = 0
        self.compression = CompressionType.NONE

    @property
    def full(self):
        return self.currentSize > 1024

    def add_content(self, content):
        self.content.append((self.currentSize, content))
        self.currentSize += len(content)
        return len(self.content) - 1

    def write(self, file):
        offsetSize = (self.currentSize.bit_length() + 7) // 8
        if self.compression == CompressionType.NONE:
            data = b"".join((c[1] for c in self.content))
        elif self.compression == CompressionType.LZ4:
            with lz4.frame.LZ4FrameCompressor() as compressor:
                data = b"".join((compressor.compress(c[1]) for c in self.content))
        elif self.compression == CompressionType.LZMA:
            compressor = lzma.LZMACompressor(format=lzma.FORMAT_XZ)
            data = b"".join((compressor.compress(c[1]) for c in self.content))
        clusterSize = ClusterHeader.nbytes + len(self.content) * offsetSize + len(data)
        header = ClusterHeader(
            compressionType=self.compression,
            clusterSize=clusterSize,
            blobCount=len(self.content),
            offsetSize=offsetSize,
        )
        file.write(header.pack())
        file.write(to_bytes(self.currentSize, offsetSize))
        for offset, _ in self.content[1:]:
            file.write(to_bytes(offset, offsetSize))
        file.write(data)
