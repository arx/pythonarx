import io
from pathlib import Path
from uuid import uuid4

from ..internal import EntryPos, PackHeader, PackInfo, PackInfoFreeData, UInt64
from .checkInfo import CheckInfo
from .cluster import Cluster


class Pack:
    # pylint: disable=too-many-instance-attributes
    def __init__(self, path, packId, vendorId, freeData):
        self.path = Path(path)
        self.file = open(self.path, "w+b")
        self.file.seek(PackHeader.nbytes, io.SEEK_SET)
        self.header = PackHeader(
            vendorId=vendorId,
            uuid=uuid4().bytes,
            packSize=0,
            checkInfoPos=0,
            entryCount=0,
            clusterCount=0,
            entryPtrPos=0,
            clusterPtrPos=0,
        )
        self.packId = packId
        self.freeData = freeData
        self.clusters = []
        self.openCluster = Cluster()
        self.blobAddresses = []

    def add_content(self, content):
        blobId = self.openCluster.add_content(content)
        self.blobAddresses.append((self.openCluster, blobId))
        if self.openCluster.full:
            self.openCluster.offset = self.file.tell()
            self.openCluster.id = len(self.clusters)
            self.openCluster.write(self.file)
            self.clusters.append(self.openCluster)
            self.openCluster = Cluster()
        return len(self.blobAddresses) - 1

    def finalize(self):
        self.header.clusterPtrPos = self.file.tell()
        self.header.clusterCount = len(self.clusters)
        for cluster in self.clusters:
            self.file.write(UInt64.pack(cluster.offset))
        self.header.entryPtrPos = self.file.tell()
        self.header.entryCount = len(self.blobAddresses)
        for cluster, blobId in self.blobAddresses:
            self.file.write(
                EntryPos.pack({"clusterNumber": cluster.id, "blobNumber": blobId})
            )

        self.header.checkInfoPos = self.file.tell()
        self.file.seek(0)
        self.file.write(self.header.pack())

        checkInfo = CheckInfo(self.file)
        checkInfo.read(self.header.checkInfoPos)
        self.file.write(bytes(checkInfo))

        packInfo = PackInfo(
            uuid=self.header.uuid,
            packId=self.packId,
            freeData=PackInfoFreeData(self.freeData),
            packSize=self.file.tell(),
            packOffset=0,
            packCheckInfoPos=self.header.checkInfoPos,
            packPath=bytes(self.path),
        )
        self.file.close()

        return packInfo, checkInfo
