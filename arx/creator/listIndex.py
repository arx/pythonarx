from ..internal import ContentAddress, KeyInfo, KeyType, ListIndexHeader


class Bytes(bytes):
    pass


class ListIndex:
    def __init__(self, name, indexKey, keys):
        self.name = bytes(name)
        self.indexKey = indexKey
        self.keyTypes = keys
        self.entries = []

    def add_entry(self, values):
        self.entries.append(
            tuple(
                Bytes(v.encode()) if (k & KeyType.pstring) else v
                for k, v in zip(self.keyTypes, values)
            )
        )

    def write(self, file):
        if self.indexKey:
            self.entries.sort(key=lambda v: v[self.indexKey])

        # We have to deduces some information from the values
        # (size of ints, extraData and size of the offsets, ...)
        # The extra data for the pstrings
        extraData = bytearray()
        # The extra information. Depends of the type of the keys (max for int, ...)
        extraInfo = [None] * len(self.keyTypes)
        for entry in self.entries:
            for index, key, value in zip(
                range(len(self.keyTypes)), self.keyTypes, entry
            ):
                if key in (KeyType.uint, KeyType.sint):
                    if extraInfo[index] is None:
                        extraInfo[index] = abs(value)
                    extraInfo[index] = max(extraInfo[index], abs(value))
                if key & KeyType.pstring:
                    value.offset = len(extraData)
                    offset = 0
                    if key & KeyType.flookup:
                        offset += 1
                    if not key & KeyType.localSize:
                        extraData.append(len(value) - offset)
                    extraData.extend(value[offset:])
        keyInfos = []
        for index, key in enumerate(self.keyTypes):
            if key == KeyType.contentAddress:
                keyInfos.append(KeyInfo(keyType=KeyType.contentAddress, keySize=0))
            elif key == KeyType.uint:
                keyInfos.append(
                    KeyInfo(
                        keyType=key, keySize=(extraInfo[index].bit_length() + 7) // 8
                    )
                )
            elif key == KeyType.sint:
                keyInfos.append(
                    KeyInfo(
                        keyType=key, keySize=((-extraInfo[index]).bit_length() + 7) // 8
                    )
                )
            elif key & KeyType.pstring:
                keySize = (len(extraData).bit_length() + 7) // 8
                if key & KeyType.localSize:
                    keySize += 1
                if key & KeyType.flookup:
                    keySize += 1
                keyInfos.append(KeyInfo(keyType=key, keySize=keySize))

        # Now it is time to write the index:
        headerSize = 42 + len(self.name) + len(keyInfos)
        header = ListIndexHeader(
            headerSize=headerSize,
            indexKey=self.indexKey,
            entrySize=sum(k.keySize if k.keySize else 4 for k in keyInfos),
            indexLength=len(self.entries),
            keysDataPos=headerSize if extraData else 0,
            keysDataLen=len(extraData),
            indexArrayPos=headerSize + len(extraData),
            extraDataCI=ContentAddress(packId=0, contentId=0),
            indexName=self.name,
            keyInfoArray=keyInfos,
        )
        file.write(header.pack())
        file.write(extraData)
        keyTypes = [k.getKeyClass("") for k in keyInfos]
        for entry in self.entries:
            for key, value in zip(keyTypes, entry):
                file.write(key.pack(value))
