import io
from hashlib import sha256

from ..internal import UInt32


class CheckInfo:
    def __init__(self, file):
        self.file = file
        self.file.seek(0)
        self.sha = sha256()

    def read(self, size):
        self.sha.update(self.file.read(size))

    def skip(self, size):
        self.file.seek(size, io.SEEK_CUR)

    def __bytes__(self):
        sha_bytes = self.sha.digest()
        return bytes(UInt32.pack(5 + len(sha_bytes)) + sha_bytes + b"\x00")
