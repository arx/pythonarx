from lzma import FORMAT_XZ, LZMADecompressor

import lz4.frame

from .internal import ClusterHeader, CompressionType, EntryPos, File, PackHeader, UInt64


class Pack:
    def __init__(self, path, pos):
        self.file = File(open(path, "r+b"))
        self.pos = pos
        self.header = PackHeader.unpack(self._fread(0, PackHeader.nbytes))
        self.entryPosArray = self.file.createArray(
            pos + self.header.entryPtrPos, EntryPos, self.header.entryCount
        )
        self.clusterPosArray = self.file.createArray(
            pos + self.header.clusterPtrPos, UInt64, self.header.clusterCount
        )

    def close(self):
        self.file.close()

    def _fread(self, offset, length):
        return self.file.read(self.pos + offset, length)

    def get_content(self, contentId):
        entryPos = self.entryPosArray[contentId]
        cluster = self._get_cluster(entryPos.clusterNumber)
        return cluster.get_blob_data(entryPos.blobNumber)

    @property
    def entryCount(self):
        return self.header.entryCount

    def _get_cluster(self, clusterId):
        clusterPos = self.clusterPosArray[clusterId]
        return Cluster(self.file, self.pos + clusterPos)


class Cluster:
    def __init__(self, file, pos):
        self.file = file
        self.pos = pos
        self.header = ClusterHeader.unpack(self._fread(0, ClusterHeader.nbytes))
        self.dataSize = self.header.offsetType.unpack(
            self._fread(ClusterHeader.nbytes, self.header.offsetSize)
        )
        self.offsetsArray = self.file.createArray(
            pos + self.header.offsetSize + ClusterHeader.nbytes,
            self.header.offsetType,
            self.header.blobCount - 1,
        )
        self.dataOffset = (
            ClusterHeader.nbytes + self.header.blobCount * self.header.offsetType.nbytes
        )
        self._data = None

    def _fread(self, offset, length):
        return self.file.read(self.pos + offset, length)

    @property
    def data(self):
        if self._data is not None:
            return self._data, 0
        if self.header.compressionType == CompressionType.NONE:
            return (
                self.file.memview(self.pos + self.dataOffset, self.dataSize),
                0,
            )
        compressedSize = self.header.clusterSize - self.header.nbytes
        self._data = b""
        offset = self.dataOffset
        if self.header.compressionType == CompressionType.LZ4:
            with lz4.frame.LZ4FrameDecompressor() as decompressor:
                while compressedSize:
                    chunckSize = min(1024, compressedSize)
                    chunck = self._fread(offset, chunckSize)
                    self._data += decompressor.decompress(chunck)
                    offset += chunckSize
                    compressedSize -= chunckSize
        elif self.header.compressionType == CompressionType.LZMA:
            decompressor = LZMADecompressor(format=FORMAT_XZ)
            while decompressor.needs_input:
                data = self._fread(offset, 1024)
                self._data += decompressor.decompress(data)
                offset += 1024
        return self._data, 0

    @property
    def blobCount(self):
        return self.header.blobCount

    def get_blob_offset(self, index):
        if index >= self.header.blobCount:
            raise IndexError
        if index == 0:
            return 0
        return self.offsetsArray[index - 1]

    def get_blob_data(self, index):
        if index >= self.header.blobCount:
            raise IndexError
        start_offset = 0 if not index else self.offsetsArray[index - 1]
        end_offset = (
            self.dataSize
            if index == self.header.blobCount - 1
            else self.offsetsArray[index]
        )
        data, offset = self.data
        return data[offset + start_offset : offset + end_offset]
