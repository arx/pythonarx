from arx.internal.utils import Array


def to_bytes(number, size):
    return number.to_bytes(size, byteorder="big")


def to_int(_bytes):
    return int.from_bytes(_bytes, byteorder="big")


class PseudoFile:
    def __init__(self, buf):
        self.buf = buf

    def memview(self, offset, size):
        return self.buf[offset : offset + size]

    def read(self, offset, size):
        return self.buf[offset : offset + size]

    def createArray(self, offset, item_type, item_count):
        return Array(self, offset, item_type, item_count)


class PseudoPack:
    # pylint: disable=too-few-public-methods
    def __init__(self, file):
        self.file = file
        self.offset = 0

    def _fread(self, offset, size):
        return self.file.read(offset, size)
