import lzma

import lz4.frame
import pytest

from arx.internal import CompressionType, PackHeader
from arx.pack import Cluster
from utils import PseudoFile, to_bytes


@pytest.fixture
def bytespackheader():
    return (
        b"arxptest\x01\x00"  # magic + version
        + b"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f"  # uuid
        + b"\xaa" * 8  # packSize
        + b"\xbb" * 8  # checksum
        + b"\x01\x02\x03"  # entryCount
        + b"\x04\x05\x06"  # clusterCount
        + b"\xcc" * 8  # entryPtrPos
        + b"\xdd" * 8  # clusterPtrPos
    )


@pytest.fixture(
    params=[CompressionType.NONE, CompressionType.LZ4, CompressionType.LZMA]
)
def compression(request):
    return request.param


class TestPackHeader:
    def test_simple(self, bytespackheader):
        # pylint: disable=redefined-outer-name, no-self-use
        header = PackHeader.unpack(bytespackheader)

        assert header.magic == 0x61727870  # b"arxp"
        assert header.vendorId == 0x74657374  # b"test"
        assert header.majorV == 1
        assert header.minorV == 0
        assert header.uuid == bytes(
            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        )
        assert header.packSize == 0xAAAAAAAAAAAAAAAA
        assert header.entryCount == 66051
        assert header.clusterCount == 263430
        assert header.entryPtrPos == 0xCCCCCCCCCCCCCCCC
        assert header.clusterPtrPos == 0xDDDDDDDDDDDDDDDD

    def test_truncated(self, bytespackheader):
        # pylint: disable=redefined-outer-name, no-self-use
        with pytest.raises(TypeError):
            PackHeader(bytespackheader[:-5])


class TestCluter:
    def test_simple(self, compression):
        # pylint: disable=redefined-outer-name, no-self-use
        datas = [
            b"A file content",
            b"Another file content.",
            (b"Some random data" + b"\x05\x00" * 100),
            b"And another file",
        ]

        data = b"".join(datas)
        if compression == CompressionType.LZ4:
            data = lz4.frame.compress(data, store_size=False)
        elif compression == CompressionType.LZMA:
            data = lzma.compress(data)
        off = 0
        offsets = []
        for d in datas:
            offsets.append(off)
            off += len(d)
        offsetSize = 2
        offsetsBuf = b"".join([to_bytes(o, offsetSize) for o in offsets[1:]])

        bufLen = 19 + len(offsetsBuf) + len(data)

        buf = (
            to_bytes(compression, 1)  # indexType
            + to_bytes(bufLen, 8)  # buf size
            + to_bytes(4, 2)  # 4 blobs
            + to_bytes(offsetSize, 1)
            + to_bytes(off, offsetSize)
            + offsetsBuf
            + data
        )

        pseudoFile = PseudoFile(buf)
        cluster = Cluster(pseudoFile, 0)

        assert cluster.header.compressionType == compression
        assert cluster.header.clusterSize == bufLen
        assert cluster.blobCount == 4
        assert cluster.dataSize == off
        assert cluster.dataOffset == 12 + offsetSize + len(offsetsBuf)

        for i in range(4):
            assert cluster.get_blob_offset(i) == 0 if not i else offsets[i]
            blobData = cluster.get_blob_data(i)
            assert blobData == datas[i]
