import pytest
from loremipsum import ParagraphLength, generate


@pytest.fixture(scope="session")
def articles():
    _articles = [("empty", 0, 0, ""), ("one", 1, 1, "1")]
    for i in range(10):
        text = generate(i, paragraph_length=ParagraphLength.MEDIUM)
        wordCount = len(text.split())
        sentenceCount = len(text.split("."))
        _articles.append((f"article{i}", sentenceCount, wordCount, text))
    return _articles
